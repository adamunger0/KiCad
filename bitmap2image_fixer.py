#!/usr/bin/env python3
#
# bitmap2image_fixer - A script to finish where Bitmap2Component falls short.
#
# Written by Adam Unger, xwhiteknuckle22@gmail.com
# File checking from: http://stackoverflow.com/questions/9532499/check-whether-a-path-is-valid-in-python-without-creating-a-file-at-the-paths-ta

import sys, argparse, fileinput, errno, os, re, shutil

# Sadly, Python fails to provide the following magic number for us.
# Windows-specific error code indicating an invalid pathname.
#
# See Also
# ----------
# https://msdn.microsoft.com/en-us/library/windows/desktop/ms681382%28v=vs.85%29.aspx
#    Official listing of all such codes.
ERROR_INVALID_NAME = 123

# the description that is displayed as part of the help or usage message
progDescription = \
"Perform basic modifications to *.kicad_mod files created in Kicad's Bitmap2Component. \
B2C provides very basic functionality and only allows selecting a subset of the layers \
on which the image should appear. As well, it incorrectly sizes the path segments such that the image will \
import and display correctly but (without an error message...) will not print or export with the rest of \
the schematic. This script is designed to address these issues and allow some tweaking of the resultant file."

def is_pathname_valid(pathname: str) -> bool:
    '''
    `True` if the passed pathname is a valid pathname for the current OS;
    `False` otherwise.
    '''

    # If this pathname is either not a string or is but is empty, this pathname
    # is invalid.
    try:
        if not isinstance(pathname, str) or not pathname:
            return False

        # Strip this pathname's Windows-specific drive specifier (e.g., `C:\`)
        # if any. Since Windows prohibits path components from containing `:`
        # characters, failing to strip this `:`-suffixed prefix would
        # erroneously invalidate all valid absolute Windows pathnames.
        _, pathname = os.path.splitdrive(pathname)

        # Directory guaranteed to exist. If the current OS is Windows, this is
        # the drive to which Windows was installed (e.g., the "%HOMEDRIVE%"
        # environment variable); else, the typical root directory.
        root_dirname = os.environ.get('HOMEDRIVE', 'C:') \
            if sys.platform == 'win32' else os.path.sep
        assert os.path.isdir(root_dirname)   # ...Murphy and her ironclad Law

        # Append a path separator to this directory if needed.
        root_dirname = root_dirname.rstrip(os.path.sep) + os.path.sep

        # Test whether each path component split from this pathname is valid or
        # not, ignoring non-existent and non-readable path components.
        for pathname_part in pathname.split(os.path.sep):
            try:
                os.lstat(root_dirname + pathname_part)
            # If an OS-specific exception is raised, its error code
            # indicates whether this pathname is valid or not. Unless this
            # is the case, this exception implies an ignorable kernel or
            # filesystem complaint (e.g., path not found or inaccessible).
            #
            # Only the following exceptions indicate invalid pathnames:
            #
            # * Instances of the Windows-specific "WindowsError" class
            #   defining the "winerror" attribute whose value is
            #   "ERROR_INVALID_NAME". Under Windows, "winerror" is more
            #   fine-grained and hence useful than the generic "errno"
            #   attribute. When a too-long pathname is passed, for example,
            #   "errno" is "ENOENT" (i.e., no such file or directory) rather
            #   than "ENAMETOOLONG" (i.e., file name too long).
            # * Instances of the cross-platform "OSError" class defining the
            #   generic "errno" attribute whose value is either:
            #   * Under most POSIX-compatible OSes, "ENAMETOOLONG".
            #   * Under some edge-case OSes (e.g., SunOS, *BSD), "ERANGE".
            except OSError as exc:
                if hasattr(exc, 'winerror'):
                    if exc.winerror == ERROR_INVALID_NAME:
                        return False
                elif exc.errno in {errno.ENAMETOOLONG, errno.ERANGE}:
                    return False
    # If a "TypeError" exception was raised, it almost certainly has the
    # error message "embedded NUL character" indicating an invalid pathname.
    except TypeError as exc:
        return False
    # If no exception was raised, all path components and hence this
    # pathname itself are valid. (Praise be to the curmudgeonly python.)
    else:
        return True
    # If any other exception was raised, this is an unrelated fatal issue
    # (e.g., a bug). Permit this exception to unwind the call stack.
    #
    # Did we mention this should be shipped with Python already?

def is_path_creatable(pathname: str) -> bool:
    '''
    `True` if the current user has sufficient permissions to create the passed
    pathname; `False` otherwise.
    '''

    # Parent directory of the passed path. If empty, we substitute the current
    # working directory (CWD) instead.
    dirname = os.path.dirname(pathname) or os.getcwd()
    return os.access(dirname, os.W_OK)

def isPathValidKicadComponent(pathname: str) -> bool:
    '''
    `True` if the passed pathname is a valid pathname for the current OS _and_
    ends in "kicad_mod"; `False` otherwise.

    This function is guaranteed to _never_ raise exceptions.
    '''

    try:
        # To prevent "os" module calls from raising undesirable exceptions on
        # invalid pathnames, is_pathname_valid() is explicitly called first.
        return is_pathname_valid(pathname) and os.path.exists(pathname) and os.path.isfile(pathname) and is_path_creatable(pathname) and (".kicad_mod" in pathname)
    # Report failure on non-fatal filesystem complaints (e.g., connection
    # timeouts, permissions issues) implying this path to be inaccessible. All
    # other exceptions are unrelated fatal issues and should not be caught here.
    except OSError:
        return False

def changeWidth(inputFileName):
    '''Bitmap2Component creates lines that are to narrow. When the component is exported from Pcbnew
       the components with these narrow lines don't export. This changes the line width to an
       experimentally determined minimum width that exports correctly. This method also creates
       a backup of the component file.'''
    
    # to change the new width modify the 2nd argument to read the desired value
    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "width" in line:
                print(re.sub("\w*(width\s+[.\w]+)", "width 0.020000", line), end="")
            else:
                print(line, end='')
    
def changeLayer(inputFileName, newLayer):
    '''Bitmap2Component doesn't allow copper layer selection for component creation. This will change the layer
       that the component is on.'''

    found_component_poly=False

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "fp_poly" in line:
                found_component_poly=True
            
            if found_component_poly and "layer" in line:
                print(re.sub("\w*(layer [.\w]+)", "layer " + newLayer, line), end="")
                found_component_poly=False
            else:
                print(line, end='')

def changeAttribute(inputFileName, newAttribute):
    '''Change the components attribute: Normal is the standard attribute. Normal+Insert indicates that the module
       must appear in the automatic insertion file (for automatic insertion machines). This attribute is most useful
       for surface mount components (SMDs). Virtual indicates that a component is directly formed by the circuit board.
       Examples would be edge connectors or inductors created by a particular track shape (as sometimes seen in
       microwave modules).'''

    found_module=False

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if found_module:
                print("  (attr " + newAttribute + ")")
                found_module=False
            if "module" in line:
                found_module=True
            if "attr" not in line:
                print(line, end='')

def changeReference(inputFileName, newReference):
    '''Change the reference identifier of the component.'''

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "fp_text reference" in line:
                print(re.sub('reference "?\w?\**"? ?\({1}', 'reference "' + newReference + '"' + " (", line), end="")
            else:
                print(line, end='')

def changeValue(inputFileName, newValue):
    '''Change the component value field.'''

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "fp_text value" in line:
                print(re.sub('value "?[\w\s]*"? ?\({1}', 'value "' + newValue + '"' + " (", line), end="")
            else:
                print(line, end='')

def changeReferenceVisibility(inputFileName, visibility):
    '''Change the reference identifier visibility of the component.'''

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "fp_text reference" in line:
                if visibility:
                    if " hide" in line:
                        print(line.replace(" hide", ""), end="")
                    else:
                        print(line, end='')
                else:
                    if not " hide" in line:
                        print(line + " hide", end="")
                    else:
                        print(line, end='')                
            else:
                print(line, end='')

def changeValueVisibility(inputFileName, visibility):
    '''Change the component value field visibility.'''

    with fileinput.FileInput(inputFileName, inplace=True) as file:
        for line in file:
            if "fp_text value" in line:
                if visibility:
                    if " hide" in line:
                        print(line.replace(" hide", ""), end="")
                    else:
                        print(line, end='')
                else:
                    if not " hide" in line:
                        print(line + " hide", end="")
                    else:
                        print(line, end='')                
            else:
                print(line, end='')

def main():
    '''The main maestro of this script. This method coordinates the entire file
    open/close/modify functionality.'''

    # start by getting the input arguments
    parser = argparse.ArgumentParser(description=progDescription)
    parser.add_argument("-l", "--layer", choices=['F.Cu', 'B.Cu', 'F.Adhes', 'B.Adhes', 'F.Paste', 'B.Paste', 'F.SilkS', 'B.SilkS', 'F.Mask', 'B.Mask', 'Dwgs.User', 'Cmts.User', 'Eco1.User', 'Eco2.User', 'Edge.Cuts', 'Margin', 'F.CrtYd', 'B.CrtYd', 'F.Fab', 'B.Fab'], help="change the components layer")
    parser.add_argument("-w", "--fix_width", action="store_true", help="change the line widths that prevent proper component exporting")
    parser.add_argument("-a", "--attribute", choices=['normal', 'normal+insert', 'virtual'], help="normal=standard, normal+insert=SMD: component must appear in insertion file for pick and place machine, virtual=component directly formed by PCB")
    parser.add_argument("-r", "--reference", help="set the reference identifier for this component")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--reference-visible", dest="reference_visibility", action="store_true", help="set the components reference field to visible")
    group.add_argument("--reference-invisible", dest="reference_visibility", action="store_false", help="set the components reference field to invisible")
    parser.set_defaults(reference_visibility=None)
    parser.add_argument("-v", "--value", help="set the value for this component")
    group = parser.add_mutually_exclusive_group()
    group.add_argument("--value-visible", dest="value_visibility", action="store_true", help="set the components value field to visible")
    group.add_argument("--value-invisible", dest="value_visibility", action="store_false", help="set the components value field to invisible")
    parser.set_defaults(value_visibility=None)
    parser.add_argument("filename", help="path to *.kicad_mod file created with Bitmap2Component")

    args = parser.parse_args()
    inputFileName = args.filename
    
    # first check for valid kicad_mod file
    if isPathValidKicadComponent(inputFileName):
        # if valid file but no options then give help to user
        if not args.fix_width and args.layer is None and args.attribute is None and args.reference is None and args.value is None and args.reference_visibility is None and args.value_visibility is None:
            print('What would you like to do to this KiCad module? Try "' + sys.argv[0], '[-h | --help]" for more information.')

        # if valid file and valid options then process this kicad module
        else:
            # start by backing up the original file
            backupFileName = inputFileName + '.bak'
            #print("Creating backup at:", backupFileName)
            #try:            
            #    shutil.copy2(inputFileName, backupFileName)
            #except Exception as e:
            #    print("failed:", e)
            #else:
            #    print("success")
            #with open(backupFileName, 'w', 0) as backupFile:
            #    with open(inputFileName, 'r', 0) as inFile:
            #        for line in inFile:
            #            backupFile.write(line)            
            
            if args.fix_width:
                print("Changing default line widths...", end='')
                try:
                    changeWidth(inputFileName)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.layer is not None:
                print("Changing layer...", end='')
                try:
                    changeLayer(inputFileName, args.layer)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.attribute is not None:
                print("Updating component attribute...", end='')
                try:
                    changeAttribute(inputFileName, args.attribute)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.reference is not None:
                print("Updating component reference...", end='')
                try:
                    changeReference(inputFileName, args.reference)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.value is not None:
                print("Updating component value...", end='')
                try:
                    changeValue(inputFileName, args.value)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.reference_visibility is not None:
                print("Updating component reference visibility...", end='')
                try:
                    changeReferenceVisibility(inputFileName, args.reference_visibility)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
            if args.value_visibility is not None:
                print("Updating component value visibility...", end='')
                try:
                    changeValueVisibility(inputFileName, args.value_visibility)
                except Exception as e:
                    print("failed:", e)
                else:
                    print("success")
    else:
        print('Invalid filename: doesn\'t exit or isn\'t a *.kicad_mod file. Try "' + sys.argv[0], '[-h | --help]" for more information.')

# if this script is called from the terminal this is run
if __name__ == "__main__":
    main()

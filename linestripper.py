#!/usr/bin/env python3

import fileinput

oldfile = '/home/aunger/Public/Projects/ChessBoard/KicadChessboard/Coil16.sch'

def main():
    numlines=0
    with fileinput.FileInput(oldfile, inplace=True) as file:
        for line in file:
            if not line.startswith('AR Path="/'):
                print(line, end='')
            else: 
                numlines = numlines+1
    print('Removed', numlines, 'number of lines.')

# if this script is called from the terminal this is run
if __name__ == "__main__":
    main()
